"""
**qecsimext** is an example Python 3 package that extends `qecsim`_ with additional components.

See the README at `qecsimext`_ for details.

.. _qecsim: https://bitbucket.org/qecsim/qecsim/
.. _qecsimext: https://bitbucket.org/qecsim/qecsimext/
"""

__version__ = '0.1b7'
